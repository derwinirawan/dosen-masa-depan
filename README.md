# Dosen Masa Depan Book Chapter

Repositori ini menyimpan file-file yang berkaitan dengan proyek penulisan buku IDRI.

File: 

1. [Draft dalam format Markdown (md)](https://gitlab.com/derwinirawan/dosen-masa-depan/-/blob/master/Dosen-masa-depan.md)
2. [Draft versi docx](https://gitlab.com/derwinirawan/dosen-masa-depan/-/blob/master/Bab%20X%20dosen%20masa%20depan-v2.docx) klik `Download`.
3. [Biodata penulis](https://gitlab.com/derwinirawan/dosen-masa-depan/-/blob/master/Biodata%20Penulis%20Dasapta%20Erwin.docx) klik `Download`.
4. [Bibliografi - Daftar Pustaka format BIBTEX](https://gitlab.com/derwinirawan/dosen-masa-depan/-/blob/master/biblio-dosen-masa-depan.bib)
5. [Bibliografi - Daftar Pustaka format RIS](https://gitlab.com/derwinirawan/dosen-masa-depan/-/blob/master/biblio-dosen-masa-depan.ris)
