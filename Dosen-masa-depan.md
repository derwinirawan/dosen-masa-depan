# Dosen masa depan

Oleh: Dasapta Erwin Irawan, Institut Teknologi Bandung / INArxiv ([ORCID](orcid.org/0000-0002-1526-0863))

## Pendahuluan

Apa yang akan saya sampaikan ini mungkin banyak yang terdengar klise, tapi pada kenyataannya tidak banyak dosen yang melaksanakannya. Alasan persisnya memang saya belum pernah ketahui, mungkin perlu ada survey khusus. Yang saya sampaikan ini merupakan pengalaman pribadi dan pengetahuan yang saya dapatkan dari berkelana di dunia maya, terutama di jagad Twitter. Oya ini adalah **saran pertama saya, `buat akun Twitter`**. Saya menyarankan ini bukan hanya untuk anda, para dosen, tapi juga untuk para mahasiswa.

Artikel ini saya tulis dengan menggunakan sesedikit mungkin referensi, karena artikel ini merupakan sebuah refleksi diri saya untuk anda. Jadi selain untuk orang lain, tulisan ini juga sebagai pengingat bagi diri saya. Secara daring anda dapat mencari rujukan terkait dengan apa yang saya sampaikan. Ada banyak rujukan tetapi kebanyakan adalah tulisan orang asing, karena memang tidak banyak dosen dari Indonesia yang sadar tentang hal ini.

Urutan pesan dan saran yang disampaikan juga tidak diurutkan secara sistematik. Urutannya berdasarkan apa yang saya pikirkan saat itu. **Spontanitas** adalah kuncinya. **Ini adalah saran kedua sebenarnya**.



## Saran pertama: jadilah dosen yang membangun profil daring dengan baik

[Saat ini profil daring bukanlah sesuatu untuk bergaya, tetapi sudah menjadi keharusan](https://www.altmetric.com/blog/tools-and-tips-to-manage-your-online-reputation/). Ini karena apapun yang anda lakukan dan hasilkan, harus dapat dicari dan ditemukan oleh orang lain secara daring. Profil daring andapun tidak boleh hanya mencantumkan jenis-jenis karya tertentu saja. Makin luas dan beragam jenisnya, makin baik. Saya menyarankan anda untuk membuat profil [ORCID](orcid.org), karena perangkat ini akan menangkap semua karya anda yang memiliki DOI. Profil [Google Scholar](scholar.google.com) juga tidak salah kalau anda buat, karena perangkat ini mengindeks seluruh dokumen anda yang daring, apapun itu. Untuk itu, rajin-rajinlah untuk mendokumentasikan kegiatan anda secara daring, bisa melalui media sosial atau blog.

Blog (misal: [Wordpress](wordpress.com) atau [Medium](medium.com)) sangat disarankan karena perangkat ini sangat mudah ditemukan menggunakan mesin pencari (misal: [Google](google.com) atau [Bing](bing.com)). Kelebihannya yang lain adalah tulisan anda akan diarsipkan sesuai dengan waktu dan akan tampil seperti itu sampai ke komputer atau gawai (_gadget_) pembaca. Agak berbeda dengan medsos (media sosial) seperti [Twitter](twitter.com) atau [Facebook](facebook.com) yang memunculkan pesan dari kita sesuai algoritma sesuai karakteristik pengunjung. Makin banyak yang mengunjungi atau menyukai, pesan kita akan makin sering muncul di urutan atas. Sayangnya ini akan menyulitkan pembaca atau anda sendiri yang akan mencari arsip pesan tertentu. Dengan blog, anda dapat mencari pesan yang anda inginkan dengan lebih mudah.

Informasi dari anda yang cukup lengkap di blog perlu disebarluaskan. Zaman sekarang blog adalah seperti sebuah alamat, kita masih perlu memberitahu pengunjung di mana kita tinggal. Di sinilah medsos seperti Twitter, Facebook, dan [Instagram](instagram.com) berperan. Salinlah tautan artikel blog ada di medsos.

Jangan lupa profil daring anda akan menggambarkan kepribadian anda. Jadi pastikan anda tidak menyumpahserapah di medsos. [Blog adalah jendela riset anda, jadi ia dapat menjadi bekal anda untuk meniti karir perdosenan](https://prezi.com/s7kbjmx72leo/blogging-and-your-academic-profile/). Saat ini pemeriksaan medsos akan selalu dilakukan oleh sebuah institusi sebelum memutuskan untuk menerima atau menolak lamaran kerja. 

Satu lagi. _Blogging_ adalah salah satu upaya untuk [membebaskan pengetahuan](https://jurnal.ugm.ac.id/bip/article/view/17054). 



## Saran kedua: jadilah dosen yang spontan

Walaupun kita sebagai kalangan akademik dididik sejak dulu untuk berhati-hati dan bekerja sesuai data, tetapi saya menyarankan anda juga harus punya spontanitas yang baik. Spontanitas anda juga harus rasional. Sejalan dengan waktu perasaan anda akan makin terasah untuk memilih dan memilah kegiatan yang perlu reaksi spontan. Ada yang bilang bahwa kekuatan pikiran akan muncul terbesar saat kita spontan atau terdesak, karena akan membuat kita mampu berpikir mendalam (_deep thinking_) dalam waktu yang pendek. [Spontanitas juga memicu kreativitas](https://www.sciencedirect.com/science/article/pii/S2352154618301396).



## Saran ketiga: jadilah dosen yang meruntuhkan "dinding"

Bagian ini saya tulis paling panjang, karena kondisi ini menurut saya paling kronis. **Dinding** yang saya maksud sangat relevan dengan kebijakan [Kampus Merdeka](https://www.kemdikbud.go.id/main/blog/2020/01/mendikbud-luncurkan-empat-kebijakan-merdeka-belajar-kampus-merdeka) dari Pak Mendikbud Nadiem. 

Dinding yang saya sebutkan ini adalah dinding kelas yang terbuat dari batubata yang membatasi ruang kelas. Dinding itu kemudian dibangun ulang sebagai dinding virtual. Dinding virtual ini akan menggantikan dinding konvensional yang sebelumnya memberikan beberapa keterbatasan ini:

* **Keterbatasan daya beli**: material yang anda berikan akan dapat membantu masyarakat yang sebelumnya tidak punya akses, atau belum punya waktu untuk mengikuti pendidikan formal, atau bagi yang ingin memperluas wawasan. Dinding virtual anda tidak boleh menghalangi orang-orang khususnya yang membutuhkan material anda untuk mendapatkan penghasilan harian. **Penghasilan harian** yang saya maksud adalah penghasilan yang akan didapatkan hanya bila bekerja setiap hari dan sepanjang hari.
* **Keterbatasan koneksi**: era digital saat ini sudah meninggalkan era modem tulalit yang dulu terkenal di awal 2000an. Kuota data adalah yang utama saat ini. Material anda harus mengakomodir juga kelompok yang tidak dapat membeli kuota data secara masif. Material video anda dengan resolusi HD (_high definition_) selama 1 jam, akan setara dengan kuota data sebesar 1 GB. Sebagai solusi, menurunkan resolusinya menjadi SD (_Standard definition_) atau menyediakan versi audio sebagai podcast. Membuat versi audio mungkin akan menurunkan kebutuhan kuota data sampai 70%. 

* **Keterbatasan daya beli**: di sini kebijaksanaan anda sebagai Makhluk Tuhan dan sekaligus sebagai Makhluk Sosial akan diuji. Di satu sisi, anda sebagai dosen juga memiliki kebutuhan dan keluarga yang harus dibiayai, tapi di sisi lain, ada banyak masyarakat yang membutuhkan pendidikan dengan kondisinya sangat jauh dari kondisi anda. Untuk itu gunakan hati nurani anda untuk memilah pengetahuan/keterampilan  yang anda bagikan, apakah bersifat generik untuk masyarakat umum atau masyarakat yang sangat membutuhkan, atau pengetahuan/keterampilan  non generik untuk masyarakat yang telah memiliki penghasilan. Saya tahu tidak ada kriteria yang mati untuk ini, tetapi anda akan dapat melakukannya dengan hati nurani. 

* **Keterbatasan teknologi**: tidak semua orang memiliki ponsel dengan memori internal 8GB dan kapasitas penyimpanan 256GB dengan layar beresolusi tinggi untuk mengikuti material daring anda. Jadi pastikan material video dan audio yang dihasilkan dapat diputar pada gawai dengan spesifikasi minimum.    

Manfaat bagi anda sudah jelas, yaitu [citra diri](https://researchwhisperer.org/tag/academic-profile/). Memang ini sering disebut sebagai pencitraan. Tapi selama itu untuk tujuan yang baik dan tidak anda lakukan secara terang-terangan serta bukan semata-mata untuk kepentingan pribadi, maka menurut saya tidak masalah.

[Tapi memang ada masalah bagi anda yang punya keterbatasan fasilitas, tapi sangat ingin berbagi](https://www.indiegogo.com/explore/community-projects?project_type=campaign&project_timing=all&sort=trending). Di era pandemi ini, seluruh kegiatan dilaksanakan secara daring, sehingga akan membutuhkan alokasi kuota data yang besar. Jadi anda perlu mendanai aktivitas anda yang berawal dari _passion_ ini. 

Untuk kebutuhan ini, anda jangan sungkan untuk menyampaikan kebutuhan ini kepada calon peserta. Ungkapkan bahwa kegiatan berbagi ilmu anda ini didorong oleh keinginan dan kesadaran pribadi tapi [anda punya keterbatasan dana untuk memenuhi kebutuhan kuota data atau pulsa internet](https://blog.prototypr.io/how-to-start-your-passion-project-and-make-time-for-it-d27687c7e47e). Saya rasa tidak ada salahnya anda memasang tarif karcis "tanda masuk" yang rasional tanpa motif mencari laba, terutama bila yang anda bagikan adalah ilmu yang bersifat generik. Intinya pertimbangan kemanusiaan perlu digunakan. Tidak ada rumus pasti, tapi silahkan anda timbang-timbang sendiri. Jangan sampai anda rajin membagikan ilmu tetapi menggerogoti keuangan keluarga. 

Kalau yang saya sampaikan anda kerjakan, maka besar kemungkinan anda akan menjadi aset yang mengharumkan nama institusi. [Anda akan jadi aset bukan beban](https://www.fnu.edu/ensure-asset-liability-workplace/). Nama institusi akan selalu terbawa, karena memang selalu anda bawa. Masyarakat akan selalu mengenal anda sebagai staf dosen/peneliti dari "institusi X". Institusi andapun akan selalu dikenal sebagai lembaga yang membawahi seluruh aktivitas anda. 

Sayangnya, hal ini jarang disadari oleh institusi anda. Alih-alih mempertahankan kinerja anda untuk [menginspirasi masyarakat dan membebaskan anda untuk tetap pada tujuan sebenarnya](https://elephantinthelab.org/translating-science-to-our-global-communities/), membagikan pengetahuan, mereka mengalihkan perhatian anda kepada hal-hal yang semu, seperti prestise karena telah menerbitkan makalah di jurnal top atau menjadi penulis dengan indeks H tertinggi, atau sebagai penulis yang paling banyak menulis makalah dan yang sejenisnya. 

Jadi kalau anda saat ini sedang menjadi pimpinan sebuah perguruan tinggi, entah pada tingkat rektorat atau dekananat, anda perlu mereset ulang pikiran anda untuk kembali ke tujuan yang sebenarnya. Penting untuk anda sadari bahwa anda sebagai pimpinan atau dosen/peneliti biasa tidak sedang membuat sebuah [pot berisi tanaman bunga, tetapi sebuah taman yang berisi buah, sayur, lumut, jamur, dan tentunya juga bunga](https://elephantinthelab.org/responsible-research-is-a-matter-of-education/).



## Saran keempat: jadilah Dosen yang tidak hanya "menerima"

Yang saya maksud dengan "hanya menerima" adalah selalu mengutamakan pemikiran kritis. Pemikiran kritis ini makin ditinggalkan menurut saya bila di ujung yang lain ada prestise dan gengsi. Tentunya ada level kritis tertentu untuk hal yang berbeda, tetapi setidaknya anda pernah mempertanyakan sesuatu, minimum satu kali, walaupun yang anda pertanyakan sudah disepakati oleh masyarakat akademik di dunia. Salah satu yang wajib anda pertanyakan adalah yang terkait dengan masalah kualitas dan reputasi akademik. Untuk anda ketahui, mayoritas yang kita anut sebenarnya adalah berbasis prestise, bukan kualitas atau reputasi, misal: masalah indeks H, Journal Impact Factor, pemeringkatan universitas dunia, dan yang sebangsanya. Bahkan saat anda ingin menyebarkan ilmu saja, anda repot sekali memilih jurnal. Ada seorang kawan saya yang mengingatkan bahwa publikasi artikel di jurnal adalah hanya cara, bukan tujuan. [Tujuan sebenarnya adalah menyebarkan ilmu pengetahuan](https://elephantinthelab.org/reconciling-research-for-the-common-good/), seperti yang tengah saya lakukan saat ini, dengan komputer milik saya, perangkat lunak [VIM yang gratis](https://www.vim.org/), serta pemikiran saya yang orisinal (untuk tingkat Indonesia). [Saya ingin menggugah kesadaran anda untuk selalu kritis](https://elephantinthelab.org/the-accuracy-of-university-rankings-in-a-international-perspective/). 



## Saran kelima: jadilah dosen yang menyemangati

Saran ini terdengar klise, tapi saya melihat banyak dosen yang sepintas [punya semangat ini](https://www.youtube.com/watch?v=Kgm5QXKoXhU), tapi di sisi lain banyak mahasiswa yang kemudian patah semangat saat keluar dari pintu ruangannya. 

Menurut saya yang anda perlu lakukan hanya mendengar sedikit lebih banyak dan sedikit lebih lama sebelum anda mengoreksi habis-habisan hasil kerja atau perilaku seorang mahasiswa. Saya menyebut ["mengoreksi" bukan "menyalahkan"](https://www.opencolleges.edu.au/informed/features/giving-student-feedback/). Artinya anda juga harus siap dengan daftar saran perbaikan. 

Tentu anda akan berpikir akan tambah repot, terutama saat anda memang sudah repot. Tenang. Mahasiswa juga kan manusia. Ia sudah cukup tenang dikala anda tidak mengeluarkan pernyataan-pernyataan yang menyakitkan. Anda tinggal memilih kata-kata yang tegas tapi menumbuhkan semangat, bukan kalimat yang pedas menyakitkan sekaligus mematahkan semangat. Saya bukan ahli psikologi jadi saya tidak bisa dan tidak dalam posisi yang dapat memberikan saran rinci. Saran saya bersifat generik saja, tinggal anda renungkan pada konteks yang berbeda-beda.    



# Penutup

Demikian beberapa saran saya untuk anda. Di luar ini pastinya ada banyak hal lain yang perlu anda perhatikan. Semoga bermanfaat bagi anda dan saya.

Salam

[@dasaptaerwin](twitter.com/dasaptaerwin)